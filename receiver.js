'use strict';

/**
 * Module dependencies.
 */

var program = require('commander'),
    security = require('./src/security'),
    socketFactory = require('socket.io-client'),
    _ = require('lodash'),
    debug = require('debug')('chat-perf:receiver');

program
    .version('0.0.1')
    .option('-s, --server [url]', 'Server [url]', 'localhost')
    .option('-p, --port [port]', 'Port [port]', '3000')
    .option('-u, --user [user]', 'User to connect as')
    .parse(process.argv);

var authenticationOptions = {
    user: program.user,
    server: program.server,
    port: program.port
};

security.authenticate(authenticationOptions, function (err, res) {
    var url = 'http://' + program.server + ':' + program.port + '/';
    var socket = socketFactory(url + '?token=' + res, {forceNew: true});
    socket.on('connect', function () {

        var recipient = undefined;
        var me = undefined;
        var messages = 0;
        var chatRoomId = undefined;

        function isChatroomMessage (chatId) {
            return (chatRoomId !== undefined && chatId === chatRoomId);
        }

        socket.on('user.updated', function (user) {
            if (user.name === program.user) {
                me = user;
            }
        });

        socket.on('chat.message', function (message) {
            debug('(receiver) got a message from: ' + message.participantId);
            setTimeout(function () {
                if (message.participantId !== me.id && !isChatroomMessage(message.chatId)) {
                    recipient = message.participantId;
                    messages++;
                    debug('(receiver) sending chat message: ' + messages);
                    socket.emit('chat.message', {chatId: message.chatId, message: '(receiver) sent message ' + Date.now()});
                }
            }, _.random(1000, 10000));
        });

        socket.on('user.left', function (user) {
            if (user === recipient) {
                debug('(receiver) exiting');
                process.exit(0);
            }
        });

        socket.on('chat.joined', function (chat) {
            if (chat.isChatRoom) {

                chatRoomId = chat.id;
                setInterval(function () {
                    if (_.random(1, 100) > 30) {
                        socket.emit('chat.message', {chatId: chat.id, message: '(receiver) chatroom message ' + Date.now()});
                    }

                }, _.random(10000, 30000))

            }
        });

        socket.emit('user.update', {status: 'online'});

        // randomly join the chatroom and post some chats
        if (_.random(1, 100) > 30 && _.contains(program.user, 'dummy')) {
            socket.emit('chatroom.join');
        }

        debug('connected');

    });

    socket.on('error', function (err) {
        console.error(err);
        throw err;
    });
});