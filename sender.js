'use strict';

/**
 * Module dependencies.
 */

var program = require('commander'),
    security = require('./src/security'),
    socketFactory = require('socket.io-client'),
    _ = require('lodash'),
    debug = require('debug')('chat-perf:sender');

program
    .version('0.0.1')
    .option('-s, --server [url]', 'Server [url]', 'localhost')
    .option('-p, --port [port]', 'Port [port]', '3000')
    .option('-u, --user [user]', 'User to connect as')
    .option('-r, --receiver [receiver]', 'User to send messages to')
    .parse(process.argv);

var authenticationOptions = {
    user: program.user,
    server: program.server,
    port: program.port
};

security.authenticate(authenticationOptions, function (err, res) {
    var url = 'http://' + program.server + ':'+ program.port +'/';
    var socket = socketFactory(url + '?token=' + res, {forceNew: true});
    socket.on('connect', function () {

        var chatId = undefined;
        var recipient = undefined;
        var messages = 0;

        socket.on('user.updated', function (user) {
            if (user.name === program.receiver) {
                debug('user joined: ' + program.receiver);
                recipient = user;
                socket.emit('chat.new', {participants: [recipient.id]});
            }
        });

        socket.on('chat.joined', function (chat) {
            chatId = chat.id;
            debug('chat joined');
            debug('sending chat message');
            socket.emit('chat.message', {chatId: chatId, message: '(sender) sent message ' + Date.now()});
        });

        socket.on('chat.message', function (message) {
            debug('(sender) got a message from: ' + message.participantId);
            setTimeout(function () {
                if (message.participantId === recipient.id) {
                    messages++;
                    debug('(sender) sending chat message: ' + messages);
                    socket.emit('chat.message', {chatId: chatId, message: '(sender) sent message ' + Date.now()});
                    if (messages == 20) {
                        debug('(sender) exiting');
                        socket.disconnect();
                        process.exit(0);
                    }
                }
            }, _.random(1000, 10000));
        });

        socket.emit('user.update', {status: 'online'});
        debug('connected');
    });

    socket.on('error', function (err) {
        console.error(err);
        throw err;
    });
});