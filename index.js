'use strict';
var debug = require('debug')('chat-perf')
var async = require('async');
var security = require('./src/security');
var socketFactory = require('socket.io-client');

var _ = require('lodash');

if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str){
        return this.slice(0, str.length) === str;
    };
}

var start = new Date();
var fiveMinutes = (5 * 60 * 1000);
var processId = process.pid;
//var hostname = 'localhost';
//var port = 3000;
var hostname = 'client-dev.chatup.io';
var port = 80;

var shouldFinish = function () {

    var diff = Math.abs(new Date() - start);
    return (diff >= fiveMinutes);
};

var conversation = function (task, callback) {

    setTimeout(function () {

        var fork = require('child_process').fork,
            sender = fork('sender.js', ['--server', hostname,'--port', port, '--user', task.sender, '--receiver', task.receiver]);
        var receiver = undefined;

        setTimeout(function () {
            receiver = fork('receiver.js', ['--server', hostname,'--port', port, '--user', task.receiver]);
            var rec2 = fork('receiver.js', ['--server', hostname,'--port', port, '--user', task.receiver + '-dummy1']);
            var rec3 = fork('receiver.js', ['--server', hostname,'--port', port, '--user', task.receiver + '-dummy2']);
            receiver.on('exit', function () {
                debug('receiver exitted.');

                rec2.kill();
                rec3.kill();

                callback();
            });
        }, 5000);

        sender.on('exit', function () { debug('sender exitted.')});

    }, _.random(100, 500));
};

//setup a chatbot for this node
var authenticationOptions = {
    user: 'Chatbot-' + processId,
    server: hostname,
    port: port
};

security.authenticate(authenticationOptions, function (err, res) {
    var url = 'http://' + hostname + ':' + port + '/';
    var socket = socketFactory(url + '?token=' + res);

    socket.on('connect', function () {

        var chatId;

        socket.on('chat.message', function (message) {
            if (message.message.startsWith('Chatbot')) {
                var tokens = message.message.split(' ');
                switch (tokens[1]) {
                    case 'ping':
                        socket.emit('chat.message', {chatId: chatId, message: 'pong'});
                        break;
                    case 'start':
                        var q = async.queue(conversation, 50);
                        q.drain = function () {
                            console.log('all items processed');
                        };

                        // create some senders and receivers
                        var senders = _.times(200, function (n) { return 'sender-' + processId +'-'+ n; });
                        var receivers = _.times(200, function (n) { return 'receiver-' + processId + '-' + n; });

                        _.forEach(senders, function (sender) {
                            var receiver = _.sample(receivers);
                            _.remove(receivers, function (r) { return r === receiver; });
                            q.push({sender: sender, receiver: receiver });
                        });

                        break;
                    case 'stop':

                        break;
                }
            }
        });

        socket.on('chat.joined', function (chat) {
            chatId = chat.id;
        });

        socket.emit('user.update', {status:'online'});
        socket.emit('chatroom.join');

        debug('emit user joined %s', 'Chatbot');
    });

    socket.on('error', function (err) {
        console.error(err);
    });
});


