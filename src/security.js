'use strict';
var http = require('http');

var Security = function () {
};

Security.prototype.authenticate = function (options, cb) {

    var loginPayload = {
        //secret: '1b1d9caec0850a8fe7a2d6b36bd67679',
        secret: 'f49ee942effdf128ecbf2586e4ed7043',
        username: options.user
    };

    var loginPayloadString = JSON.stringify(loginPayload);

    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': loginPayloadString.length
    };

    var requestOptions = {
        hostname: options.server,
        port: options.port,
        path: '/authenticate',
        method: 'POST',
        headers: headers
    };

    var req = http.request(requestOptions, function (res) {
        res.setEncoding('utf-8');

        var token = '';

        res.on('data', function (data) {
            token += data;
        });

        res.on('end', function () {
            process.nextTick(function () {
                cb(null, token);
            });
        });
    });

    req.on('error', function (e) {

        process.nextTick(function () {
            cb(e, null);
        });

    });

    req.write(loginPayloadString);
    req.end();

};

module.exports = new Security();