'use strict';

var rest = require('restler');

var UserService = function (baseUrl, token) {

    this.Service = rest.service(function () {
        this.defaults.accessToken = token;
    }, {
        baseURL: baseUrl
    });
};

UserService.prototype.find = function (username, cb) {

    var service = new this.Service();
    service.get('/api/users?name=' + username)
        .on('complete', function (data, res) {
            process.nextTick(function () {
                cb(null, data);
            })
        });
};

module.exports = UserService;